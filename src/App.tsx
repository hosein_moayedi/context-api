import React from "react";
import "./App.css";
import { AppProvider, UserProvider } from "./contexts/index";
import Login from "./Login";

const App = () => {
  return (
    <AppProvider>
      <UserProvider>
        <Login />
      </UserProvider>
    </AppProvider>
  );
};

export default App;
