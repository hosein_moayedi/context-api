import { Dispatch } from 'react';
import ContextCreator from '../../ContextCreator';
import { ActionsType, StateType } from './models';



const state: StateType = {
  connnection: false,
  appVersion: '1.0',
  loading: false
}



const actionCreator = (dispatch?: Dispatch<StateType>): ActionsType => {
  return { setAppState: (value) => dispatch && dispatch(value) }
}

export const { Context, Provider } = ContextCreator<StateType, ActionsType>(state, actionCreator)