export type StateType = {
    connnection?: boolean,
    appVersion?: string,
    loading?: boolean
}


export type ActionsType = {
    setAppState: (value: StateType) => void,
}
