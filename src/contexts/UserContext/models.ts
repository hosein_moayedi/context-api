export type StateType = {
    id?: number,
    avatar?: string | null,
    fullName?: string
}


export type ActionsType = {
    setUserState: (value: StateType) => void,
}
