import { Dispatch } from 'react';
import ContextCreator from '../../ContextCreator';
import { ActionsType, StateType } from './models';



const state: StateType = {
  id: 0,
  avatar: null,
  fullName: ''
}



const actionCreator = (dispatch?: Dispatch<StateType>): ActionsType => {
  return { setUserState: (value) => dispatch && dispatch(value) }
}

export const { Context, Provider } = ContextCreator<StateType, ActionsType>(state, actionCreator)