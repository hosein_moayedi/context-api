import React, { useContext, useEffect } from "react";
import { AppContext, UserContext } from "./contexts/index";

const Login = () => {
  const { appVersion, setAppState } = useContext(AppContext)
  const { id, setUserState } = useContext(UserContext)

  useEffect(() => {
    setAppState({ appVersion: 'v12.0' })
    setUserState({ id: 1 })
  }, []);

  return <>
    <h1>{appVersion}</h1>
    <h1>{id}</h1>
  </>;
};

export default Login;
