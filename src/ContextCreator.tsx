import React, { createContext, Dispatch, ReactElement, useReducer } from 'react';


export default function ContextCreator<T, G>(initialState: T, actionCreator: (dispatch?: Dispatch<T>) => G) {

    const Context = createContext({ ...initialState, ...actionCreator() })

    const Provider = ({ children }: { children: ReactElement }) => {

        const reducer = (state: T & G, value: T) => ({ ...state, ...value })

        const [state, dispatch] = useReducer(reducer, { ...initialState, ...actionCreator() })


        return (
            <Context.Provider value={{ ...state, ...actionCreator(dispatch) }}>
                {children}
            </Context.Provider>
        )
    }

    return { Context, Provider }
}
